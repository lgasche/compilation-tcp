# Rapport AS 
GASCHE Luca HANQUEZ Maël

Ce projet est un compilateur pour le langage TPC, écrit en C utilisant GNU Flex et GNU Bison.

## Building
Flex `2.6` ou plus récent et GNU Bison` 3.3` ou plus récent sont requis pour le build.
Configurez et construisez le projet à l'aide des commandes standard `make`:
```console
$ make clean
$ make
```

Pour exécuter l'ensemble des tests invoquer la commande :
```console
$ make test
```
Pour exécuter les tests fonctionnelles invoquer les commandes :
```console
$ cd build
$ memcheck
```
Pour exécuter les tests de mémoires invoquer les commandes :
```console
$ cd build
$ make memcheck
```
Les sorties standars et d'erreur des tests est consultable dans le fichier **ProjetASL3_GASCHE_HANQUEZ/build/test/LastTest.log** .

## Détails d'implémentation
### Variables globales et fuite mémoires
Les outils Bison et Flex utilisent par défaut des variables globales pour communiquer.
Suite à la mise en place des tests de mémoires (memcheck) avec l'outil valgrind, nous avons constater des fuites de mémoires du a des oublis de libération de zone mémoires alloué sur le tas par bison et affécter à des variables globales.
Pour palier a ce problème, nous avons choisit d'utiliser l'api reetrant de bison.
La mise en place de cette option permet de remplacer les variables globales habituellement générer par des structures de contexte ayant pour objectif la sauvegarde de l'état du parser et du lexer, et dont l'allocation et la libération sont à la charge de l'utilisateur.

### Le 3eme acteurs parser.c
Suite à ce choix, un autre problème est apparu : la dépendance circulaire entre flex et bison.
En effet, l'utilisation de l'api reetrant implique la modification de la fonction yylex, ce qui complique la configurations des deux outils en question.
L'idée est donc de supprimer cette dépendance circulaire via un 3eme acteurs, dont le but est d'interagir avec flex pour récupérer les token et les transmettre à bison pour le parsing. (Le comportement par défaut de bison est d'interroger par lui même yylex afin d'obtenir le token suivant).
Il est possible d'obtenir ce comportement en utilisant l'api push de bison.
Dans notre cas le fichier parser.c contient l'implémentation de ce 3eme acteur, effectuant l'initialisation et la libération des structure de contexts, et servant d'intermédiaire entre flex et bison.

### L'api location de bison
Celle-ci permet de transmettre des informations correspondantes aux numéro de lignes et de colonnes.
Son intérêt est de pouvoir garder en temps réel une information sur la position du dernier token lu.
Et donc si problème, de pouvoir localiser l'erreur précisément à l'aide de ses coordonnées (ligne/colonnes).

### YY_USEUR_ACTION flex
La macro YY_USEUR_ACTION de flex permet de dupliquer du code implicitement.
Pour chaque token envoyé à bison, nous lui transmettons ses coordonnées :
```flex
#define YY_USER_ACTION  yylloc->first_line = yylloc->last_line;\
                        yylloc->last_line = yylineno;\
                        yylloc->first_column= yylloc->last_column;\
                        yylloc->last_column += yyleng;
}
```

### Règles particulières
#### EOF
```flex
<<EOF>>                             { YY_USER_ACTION; yyterminate(); }
```
**<<EOF>>** est une règle qui peut être implicite avec une utilisation standard de flex et bison, or lors de l'utilisation de l'api push celle-ci devient obligatoire afin de prévenir de l'arrêt du parser.
Il faut tout fois faire attention à un cas particulier de YY_USER_ACTION, celui-ci ne recopie rien pour EOF si cela n'est pas clairement spécifié dans la règle.

#### CHARACTER
```flex
'([[:print:]]{-}[\\]|\\[\\'nt])'    { return CHARACTER; }
```
**CHARACTER** permet de trouver les caractères **printable** en C mais aussi les caractères d’échappement tel que : \\t, \\n, \\' et \\\ .
Mais ne doit pas autoriser les littérales : retour à la ligne, tabulation, ect..
Elle se distingue des autres règles par sa décomposition en deux parties :
1) Elle ne rencontre pas de barre oblique inversée, donc c'est un caractère printable accepter.
2) Elle rencontre une barre oblique inversée, les seuls cas possibles autorisés après cela sont : \\t, \\n, \\' et \\\ .

### Conflit shift/reduce
La grammaire du projet possède un conflit bien connue nommé **dangling else problem**.
Celui-ci met en lumière l'ambiguïté de l'interprétation de la clause **sinon** dans l'imbrication de deux instructions conditionnelles de la forme **si-alors-sinon**.
Via l'option **bison --verbose ../src/parser.y**, nous pouvons accéder à l'automate de la grammaire :
```
État 131

   28 Instr: IF '(' Exp ')' Instr .
   29      | IF '(' Exp ')' Instr . ELSE Instr

    ELSE  décalage et aller à l'état 134

    ELSE      [réduction par utilisation de la règle 28 (Instr)]
    $défaut  réduction par utilisation de la règle 28 (Instr)
```
Ici bison nous indique qu'il est apte à trouver deux solutions pour le **IF/ELSE**.
Afin de remédier à ce souci, il suffit de spécifier à bison ce que nous voulons exactement et donc de ne pas lui laisser le choix, en ajoutant avant la grammaire : **%right ')' ELSE** afin de rattacher le ELSE au dernier IF lu.

## Ressources
* https://westes.github.io/flex/manual
* https://www.gnu.org/software/bison/manual/html_node