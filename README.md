# AS
 
This project is a Tpclang compiler written in C using Flex and GNU Bison.
 
## Building
 
Flex `2.6` or newer and GNU Bison `3.3` or newer are required for building.
 
Configure and build the project using standard `make` commands:
 
```console
$ make
```

To run the test suite, invoke:

```console
$ make test
```
