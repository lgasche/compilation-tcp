BUILD=$(PWD)/build

.PHONY: build
build:
	cmake -B '$(BUILD)'
	cmake --build '$(BUILD)'
	cp '$(BUILD)/src/as' .

.PHONY: test
test: build
	cmake --build '$(BUILD)' --target test

.PHONY: clean
clean:
	rm -rf '$(BUILD)' as
