#include <stdio.h>
#include <stdlib.h>
#include "parser.h"

int main(int argc, char *argv[]) {
  FILE *input;
  int status;
  input = stdin;
  if (argc >= 2) {
    input = fopen(argv[1], "r");
    if (input == NULL) {
      fprintf(stderr, "can't open file: %s\n", argv[1]);
      return EXIT_FAILURE;
    }
  }
  status = parse_file(input);
  fclose(input);
  return status == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
