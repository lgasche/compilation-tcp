#include <stdio.h>

#include "generated_lexer.h"
#include "generated_parser.h"
#include "parser.h"

static int printf_syntax_error(FILE *input, const struct error_s *error) {
  char *buf = NULL;
  int begin, end, size;
  int line = error->location.first_line;
  int position = error->location.first_position;
  if (fseek(input, position, SEEK_SET) == 0) {
    if (line == 1) {
      begin = 0;
    } 
    else {
      while (fgetc(input) != '\n') {
        fseek(input, -2, SEEK_CUR);
      }
      begin = ftell(input);
    }
    fseek(input, begin, SEEK_SET);
    size = (position - begin) * 2 + 1;
    buf = malloc(size);
    if (buf == NULL) {
      return 1;
    }
    fgets(buf, size, input);
    end = strlen(buf);
    fprintf(stderr, "%d:%d\n", line, position - begin);
    if (end != 0) {
      fprintf(stderr, "%.*s\n", end - (buf[end - 1] == '\n'), buf);
      fprintf(stderr, "%*c", (position - begin), ' ');
    }
    else{
      fprintf(stderr, "%s\n", buf);
    }
    fprintf(stderr, "^\n");
  }
  free(buf);
  return 0;
}

int parse_file(FILE *input) {
  YY_BUFFER_STATE buf;
  int status;
  struct error_s error;
  yyscan_t scanner;
  /* State parser. */
  yypstate *ps;
  YYLTYPE location = {1, 0, 1, 0};
  yylex_init(&scanner);
  /* When in doubt, use YY_BUF_SIZE for the size. */
  buf = yy_create_buffer(input, YY_BUF_SIZE, scanner);
  yy_switch_to_buffer(buf, scanner);
  ps = yypstate_new();
  do {
    status = yypush_parse(ps, yylex(NULL, &location, scanner), NULL, &location, &error);
  } while(status == YYPUSH_MORE);
  if (status == 1) {
    if (error.type == SYNTAX_ERROR) {
      if (printf_syntax_error(input, &error) == 1) {
        fprintf(stderr, "Out of memory buf.\n");
      }
    }
    else {
      fprintf(stderr, "Out of memory.\n");
    }
  }
  yy_delete_buffer(buf, scanner);
  yylex_destroy(scanner);
  yypstate_delete(ps);
  return status;
}
