%define api.pure full
%define api.push-pull push
%locations
%parse-param {struct error_s *error}

%code requires {
#include "parser.h"
}

%code top {
#include <string.h>
}

%code {
void yyerror(YYLTYPE *llocp, struct error_s *error, const char* msg);
}

%token TYPE
%token IDENT
%token VOID
%token READE
%token READC
%token PRINT
%token IF
%token ELSE
%token WHILE
%token RETURN
%token OR
%token AND
%token EQ
%token ORDER
%token ADDSUB
%token DIVSTAR
%token NUM
%token CHARACTER
%token STRUCT

%right ')' ELSE

%%

Prog : DeclStructs DeclVars DeclFoncts
     | DeclStructs DeclFoncts
     ;

DeclStructs : DeclStructs STRUCT IDENT '{' DeclVars '}' ';'
            | %empty
            ;

DeclVars : DeclVars Type Declarateurs ';'
         | Type Declarateurs ';'
         ;

Type : TYPE
     | STRUCT IDENT
     ;

Declarateurs : Declarateurs ',' IDENT
             | IDENT 
             ;

DeclFoncts : DeclFoncts DeclFonct;
           | DeclFonct
           ;

DeclFonct : EnTeteFonct Corps
          ;

EnTeteFonct : Type IDENT '(' Parametres ')'
            | VOID IDENT '(' Parametres ')'
            ;

Parametres : VOID
           | ListTypVar
           ;

ListTypVar : ListTypVar ',' Type IDENT
           | Type IDENT
           ;

Corps : '{' DeclVars SuiteInstr '}'
      | '{' SuiteInstr '}'
      ;

SuiteInstr : SuiteInstr Instr
           | %empty
           ;

Instr : LValue '=' Exp ';'
      | READE '(' IDENT ')' ';'
      | READC '(' IDENT ')' ';'
      | PRINT '(' Exp ')' ';'
      | IF '(' Exp ')' Instr
      | IF '(' Exp ')' Instr ELSE Instr
      | WHILE '(' Exp ')' Instr
      | IDENT '(' Arguments ')' ';'
      | RETURN Exp ';'
      | RETURN ';'
      | '{' SuiteInstr '}'
      | ';'
      ;

Exp : Exp OR TB
    | TB
    ;

TB : TB AND FB
   | FB
   ;

FB : FB EQ M
   | M
   ;

M : M ORDER E
  | E
  ;

E : E ADDSUB T
  | T
  ;

T : T DIVSTAR F
  | F
  ;

F : ADDSUB F
  | '!' F
  | '(' Exp ')'
  | NUM
  | CHARACTER
  | LValue
  | IDENT '(' Arguments ')'
  ;

LValue : IDENT 
       ;

Arguments : ListExp
          | %empty
          ;

ListExp : ListExp ',' Exp
        | Exp
        ;

%%

void yyerror(YYLTYPE *llocp, struct error_s *error, const char *msg) {
  if (strcmp(msg, "syntax error") == 0) {
    error->type = SYNTAX_ERROR;
    error->location.first_line = llocp->first_line;
    error->location.last_line = llocp->last_line;
    error->location.first_position = llocp->first_column;
    error->location.last_position = llocp->last_column;
  }
  else {
    error->type = OUT_OF_MEMORY;
  }
}
