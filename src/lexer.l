%option nounput
%option noinput
%option nodefault
%option noyywrap
%option never-interactive
%option yylineno
%option reentrant
%option bison-bridge bison-locations

%top{
#include "generated_parser.h"

#define YY_USER_ACTION                                                         \
  {                                                                            \
    yylloc->first_line = yylloc->last_line;                                    \
    yylloc->last_line = yylineno;                                              \
    yylloc->first_column = yylloc->last_column;                                \
    yylloc->last_column += yyleng;                                             \
  }
}

%%

"//".*\n?                           { /* Consumed. */ }
"/*"([^*]|\*[^/])*"*/"              { /* Consumed. */ }
[[:space:]]+                        { /* Consumed. */ }

char|int                            { return TYPE; }
void                                { return VOID; } 
reade                               { return READE; }
readc                               { return READC; }
print                               { return PRINT; }
if                                  { return IF; }
else                                { return ELSE; }
while                               { return WHILE; }
return                              { return RETURN; }
struct                              { return STRUCT; }
[[:alpha:]][[:alnum:]_]{0,30}       { return IDENT; }
"||"                                { return OR; }
&&                                  { return AND; }
[!=]=                               { return EQ; }
[<>]=?                              { return ORDER; }
[+-]                                { return ADDSUB; }
[/*%]                               { return DIVSTAR; }
[[:digit:]]+                        { return NUM; }
'([[:print:]]{-}[\\]|\\[\\'nt])'    { return CHARACTER; }
.                                   { return yytext[0]; }
<<EOF>>                             { YY_USER_ACTION; yyterminate(); }

%%
