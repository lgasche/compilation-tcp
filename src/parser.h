#ifndef PARSER_H_
#define PARSER_H_

#include <stdio.h>

struct location_s {
  int first_line;
  int last_line;
  int first_position;
  int last_position;
};

enum error_type_e { SYNTAX_ERROR, OUT_OF_MEMORY };

struct error_s {
  enum error_type_e type;
  struct location_s location;
};

/**
 * Parses the TPC program file in the stream input.
 * 
 * @param FILE *input
 * @return value of the parser equal to 0 if and only if the entry is correct, else 1.
 */
int parse_file(FILE *input);

#endif /* PARSER_H_ */
